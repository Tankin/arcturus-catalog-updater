/**
 * Created on 2-5-2015 14:11.
 */
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

public class DatabasePool
{
    private HikariDataSource database;

    public boolean getStoragePooling()
    {
        try
        {
            HikariConfig databaseConfiguration = new HikariConfig();
            databaseConfiguration.setMaximumPoolSize(50);
            databaseConfiguration.setInitializationFailFast(true);
            databaseConfiguration.setDataSourceClassName("com.mysql.jdbc.jdbc2.optional.MysqlDataSource");
            databaseConfiguration.addDataSourceProperty("serverName", CatalogUpdater.configurationManager.getValue("db.hostname"));
            databaseConfiguration.addDataSourceProperty("port", CatalogUpdater.configurationManager.getValue("db.port", "3306"));
            databaseConfiguration.addDataSourceProperty("databaseName", CatalogUpdater.configurationManager.getValue("db.database"));
            databaseConfiguration.addDataSourceProperty("user", CatalogUpdater.configurationManager.getValue("db.username"));
            databaseConfiguration.addDataSourceProperty("password", CatalogUpdater.configurationManager.getValue("db.password"));
            databaseConfiguration.setAutoCommit(true);
            databaseConfiguration.setConnectionTimeout(34000L);
            databaseConfiguration.setLeakDetectionThreshold(90000L);
            databaseConfiguration.setMaxLifetime(2874L);
            databaseConfiguration.setIdleTimeout(2874L);
            this.database = new HikariDataSource(databaseConfiguration);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public HikariDataSource getDatabase()
    {
        return this.database;
    }
}
